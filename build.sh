#! /bin/bash

#
#
#

buildhome=$(pwd)

test -e build_config || { echo "Cannot find build_config. Use it to set local path to hbase repo"; exit 1; }

source build_config

# check for protobuf
protoc=$(which protoc)
test -x "$protoc" || { echo "protobuf-compiler protoc seems to be missing."; exit 1; }
echo -e "Using protoc: ${protoc} $(protoc --version)";

test -d "$GIT_HBASE_HOME" || {
	# initialize hbase repo
	cd $(dirname $GIT_HBASE_HOME)
	echo Working directory to checkout hbase is: $(pwd)
	git clone --verbose https://github.com/apache/hbase
}

test -d "$GIT_HBASE_HOME" || {
	echo "Failed to git clone hbase. Whoops...";
	exit 1;
}
# update hbase repo and build hbase

test -e "$GIT_HBASE_HOME/target/hbase-$HBASE_VERSION.tar.gz" || {

	hbasepatchfile="$(pwd)/patches/hbase-${HBASE_VERSION}.patch"

	# BUILD HBASE TAR BALL

	cd "$GIT_HBASE_HOME" ; 
	
	git checkout "$HBASE_VERSION" ;

	test -e "$hbasepatchfile" && {
		echo "Patching hbase..."
		patch -p1 < "$hbasepatchfile"
	}

	protoc -Isrc/main/protobuf --java_out=src/main/java src/main/protobuf/hbase.proto
	protoc -Isrc/main/protobuf --java_out=src/main/java src/main/protobuf/ErrorHandling.proto

	mvn clean install -DskipTests -Dhadoop.profile=2.0 -Prelease ;
	
	# REMOVE hadoop.profile=2.0 WHEN BUILDING HBASE 0.98+
	mvn install -Dhadoop.profile=2.0 -DskipTests site assembly:single -Prelease
}

test -e "$GIT_HBASE_HOME/target/hbase-$HBASE_VERSION.tar.gz" || {
	echo "It seems the build process was not successful";
	exit 1;
}

cd "$buildhome"

echo "# copying hbase binary tar ball to hbase.tar.gz"
cp "$GIT_HBASE_HOME/target/hbase-$HBASE_VERSION.tar.gz" "hbase.tar.gz"

sed -e "s/@HBASE_VERSION@/${HBASE_VERSION}/" < Dockerfile.dist > Dockerfile

dockertag="aptlygmbh/hadoop-aws-docker:hadoop240-hbase${HBASE_VERSION}"
set -x
sudo docker build -t "$dockertag" .

echo
echo "# built docker image ${dockertag}"

# cleanup
#rm Dockerfile



