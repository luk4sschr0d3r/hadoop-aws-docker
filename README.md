Hadoop + HBase in Docker
==============================================
... trying to mimic (Amazon EMR 3.3.1)

This configuration builds a docker container to run HBase (with
embedded Zookeeper) running on the files inside the container.

The versions chosen for Hadoop and HBase mimic Amazon EMR.
As of this writing the latest AMI is 3.3.1, providing Hadoop 2.4.0 and HBase 0.94.18.

This image provides
- Hadoop2 2.6.0
- HBase 0.94.26 (latest 0.94 version)

It is based on `https://github.com/sequenceiq/hadoop-docker` and `https://github.com/dajobe/hbase-docker`.


BUILD REQUIREMENTS
------------------

- protoc - v2.5+
- Maven 3.0.5+
- Java 1.6 (HBase 0.94. depends on Java 6 (tools.jar 1.6))


Build process
-------------

HBase 0.94 by default is compiled against Hadoop1. In order to be compatible to Hadoop2, HBase needs to be built using mvn3 with the -Dhadoop.profile=2.0 option.

To achieve this, we need a local clone of the Apache HBase git repository. The ./build.sh script will use your local copy of Apache HBase and compile it.

- copy build\_config.example to build\_config and edit this file to provide the path you (want to) keep your local copy of Apache HBase sources.

Then, start the build using

	$ ./build.sh


Running the image
-----------------

After a successful build run the container using

	$ sudo ./run.sh

> The current approach requires editing the local server's `/etc/hosts`
> file to add an entry for the container hostname and IP mapped to the fixed
> name of `hadoop-aws-docker`.  This is because
> HBase uses hostnames to pass connection data back out of the
> container (from it's internal Zookeeper).

Hopefully this can be fixed when newer Docker allows more advanced
networking such as fixed IPs or dynamic registration of name/IP
mappings (avahi?).


See HBase Logs
--------------

If you want to see the latest logs live use:

    $ docker attach $id

Then ^C to detach.

To see all the logs since the HBase server started, use:

    $ docker logs $id

and ^C to detach again.

To see the hbase thrift and server logs; use `start-hbase.sh` and
they will be written to the local directory `logs/` using a volume
mapping.


Test HBase is working from Java
-------------------------------

This requires using the `start-hbase.sh` approach and running in the
HBase source tree (might require `JAVA_HOME` to be set).

	$ bin/hbase shell
	HBase Shell; enter 'help<RETURN>' for list of supported commands.
	Type "exit<RETURN>" to leave the HBase Shell
	Version 0.94.11, r1513697, Wed Aug 14 04:54:46 UTC 2013

	hbase(main):001:0> status
	1 servers, 0 dead, 3.0000 average load

	hbase(main):002:0> list
	TABLE
	table-name
	1 row(s) in 0.0460 seconds

Showing the `table-name` table made in the happybase example above.


Proxy HBase UIs locally
-----------------------

If you are running docker on a remote machine, it is handy to see
these server-private urls in a local browser so here is a
~/.ssh/config fragment to do that

    Host hadoop-aws-docker
	Hostname 1.2.3.4
    LocalForward 127.0.0.1:$master_ui_port 127.0.0.1:60010
    LocalForward 127.0.0.1:$region_ui_port 127.0.0.1:60030
    LocalForward 127.0.0.1:$thrift_ui_port 127.0.0.1:9095

When you `ssh hadoop-aws-docker` ssh connects to the docker server and
forwards request on your local machine on ports 60010 / 60030 to the
remote ports that are attached to the hbase container.

The downside above is you have to edit the SSH config file every time
you restart docker because the ports will have changed.

The bottom line, you can use these URLs to see what's going on:

  * http://localhost:60010/master-status for the Master Server
  * http://localhost:60030/rs-status for Region Server
  * http://localhost:9095/thrift.jsp for the thrift UI
  * http://localhost:60030/zk.jsp for the embedded Zookeeper

to see what's going on in the container and since both your local
machine and the container are using localhost (aka 127.0.0.1), even
the links work!





Notes
-----

[1] http://happybase.readthedocs.org/en/latest/

[2] https://github.com/wbolster/happybase

