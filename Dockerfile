# Hadoop and HBase in Docker
#
# Version 0.1

# http://docs.docker.io/en/latest/use/builder/

FROM ubuntu
MAINTAINER Lukas Schroeder <lschroeder@aptly.de>

# make sure the package repository is up to date
RUN sed 's/main$/main universe/' -i /etc/apt/sources.list

# Install build requirements
RUN apt-get install -y tar sudo openssh-server openssh-client rsync alien
RUN apt-get install -y curl #openjdk-6-jdk

# passwordless ssh
RUN rm /etc/ssh/ssh_host_dsa_key && rm /etc/ssh/ssh_host_rsa_key 
RUN ssh-keygen -q -N "" -t dsa -f /etc/ssh/ssh_host_dsa_key
RUN ssh-keygen -q -N "" -t rsa -f /etc/ssh/ssh_host_rsa_key
RUN ssh-keygen -q -N "" -t rsa -f /root/.ssh/id_rsa
RUN cp /root/.ssh/id_rsa.pub /root/.ssh/authorized_keys

ADD ssh_config /root/.ssh/config
RUN chmod 600 /root/.ssh/config
RUN chown root:root /root/.ssh/config

# fix the 254 error code
RUN sed  -i "/^[^#]*UsePAM/ s/.*/#&/"  /etc/ssh/sshd_config
RUN echo "UsePAM no" >> /etc/ssh/sshd_config
RUN echo "Port 2122" >> /etc/ssh/sshd_config

# start ssh daemon, or starting hadoop fails because of dependency on password-less ssh
RUN service ssh start


# java
RUN cd /tmp && curl -LO 'http://download.oracle.com/otn-pub/java/jdk/7u51-b13/jdk-7u51-linux-x64.rpm' -H 'Cookie: oraclelicense=accept-securebackup-cookie'
#ADD jdk-7u51-linux-x64.rpm /tmp/jdk-7u51-linux-x64.rpm
#RUN rpm -i jdk-7u51-linux-x64.rpm
RUN echo "Installing ORACLE Java - will take some time. Converting from .rpm using alien" && alien --scripts -i /tmp/jdk-7u51-linux-x64.rpm
RUN rm /tmp/jdk-7u51-linux-x64.rpm

ENV JAVA_HOME /usr/java/default
ENV PATH $PATH:$JAVA_HOME/bin

# hadoop
RUN curl -s http://www.eu.apache.org/dist/hadoop/common/hadoop-2.6.0/hadoop-2.6.0.tar.gz | tar -xz -C /usr/local/
RUN cd /usr/local && ln -s ./hadoop-2.6.0 hadoop

ENV HADOOP_PREFIX /usr/local/hadoop
ENV HADOOP_COMMON_HOME /usr/local/hadoop
ENV HADOOP_HDFS_HOME /usr/local/hadoop
ENV HADOOP_MAPRED_HOME /usr/local/hadoop
ENV HADOOP_YARN_HOME /usr/local/hadoop
ENV HADOOP_CONF_DIR /usr/local/hadoop/etc/hadoop
ENV YARN_CONF_DIR $HADOOP_PREFIX/etc/hadoop

RUN sed -i '/^export JAVA_HOME/ s:.*:export JAVA_HOME=/usr/java/default\nexport HADOOP_PREFIX=/usr/local/hadoop\nexport HADOOP_HOME=/usr/local/hadoop\n:' $HADOOP_PREFIX/etc/hadoop/hadoop-env.sh
RUN sed -i '/^export HADOOP_CONF_DIR/ s:.*:export HADOOP_CONF_DIR=/usr/local/hadoop/etc/hadoop/:' $HADOOP_PREFIX/etc/hadoop/hadoop-env.sh
#RUN . $HADOOP_PREFIX/etc/hadoop/hadoop-env.sh

RUN mkdir $HADOOP_PREFIX/input
RUN cp $HADOOP_PREFIX/etc/hadoop/*.xml $HADOOP_PREFIX/input

# pseudo distributed
ADD core-site.xml.template $HADOOP_PREFIX/etc/hadoop/core-site.xml.template
RUN sed s/HOSTNAME/localhost/ /usr/local/hadoop/etc/hadoop/core-site.xml.template > /usr/local/hadoop/etc/hadoop/core-site.xml
ADD hdfs-site.xml $HADOOP_PREFIX/etc/hadoop/hdfs-site.xml

ADD mapred-site.xml $HADOOP_PREFIX/etc/hadoop/mapred-site.xml
ADD yarn-site.xml $HADOOP_PREFIX/etc/hadoop/yarn-site.xml

VOLUME ["/dfs", "/data"]
# /dfs is for hdfs
# /data is for hbase.zookeeper.property.dataDir
RUN mkdir -p /dfs /data
RUN $HADOOP_PREFIX/bin/hdfs namenode -format

# fixing the libhadoop.so like a boss (centos based sequenceiq compile)
#RUN rm  /usr/local/hadoop/lib/native/*
#RUN curl -Ls http://dl.bintray.com/sequenceiq/sequenceiq-bin/hadoop-native-64-2.6.0.tar|tar -x -C /usr/local/hadoop/lib/native/


# Install HBase
ENV HBASE_VERSION 0.94.26

RUN mkdir -p /opt/downloads
# If the <src> parameter of ADD is an archive in a recognised compression format, it will be unpacked
ADD hbase.tar.gz /opt/downloads
RUN mv /opt/downloads/hbase-$HBASE_VERSION /opt/hbase


# Data will go here (see hbase-site.xml)
RUN mkdir -p /data /opt/hbase/logs

ENV JAVA_HOME /usr/java/default
ENV HBASE_SERVER /opt/hbase/bin/hbase

ADD ./hbase-site.xml /opt/hbase/conf/hbase-site.xml.template

ADD ./zoo.cfg /opt/hbase/conf/zoo.cfg.template

ADD ./hbase-server /opt/hbase-server


RUN echo "Preparing bootstrap.sh" && rm -f /etc/bootstrap.sh
ADD bootstrap.sh /etc/bootstrap.sh
RUN chown root:root /etc/bootstrap.sh
RUN chmod 700 /etc/bootstrap.sh

ENV BOOTSTRAP /etc/bootstrap.sh

# workingaround docker.io build error
RUN ls -la /usr/local/hadoop/etc/hadoop/*-env.sh
RUN chmod +x /usr/local/hadoop/etc/hadoop/*-env.sh
RUN ls -la /usr/local/hadoop/etc/hadoop/*-env.sh

RUN echo "export JAVA_HOME=/usr/java/default" >> /opt/hbase/conf/hbase-env.sh

#RUN $HADOOP_PREFIX/etc/hadoop/hadoop-env.sh && $HADOOP_PREFIX/sbin/start-dfs.sh && $HADOOP_PREFIX/bin/hdfs dfs -put $HADOOP_PREFIX/etc/hadoop/ input


# HBASE PORTS
# Thrift API
EXPOSE 9090
# Thrift Web UI
EXPOSE 9095
# HBase's zookeeper - used to find servers
EXPOSE 2181
# HBase Master API port
EXPOSE 60000
# HBase Master web UI at :60010/master-status;  ZK at :60010/zk.jsp
EXPOSE 60010
# Region server API port
EXPOSE 60020
# HBase Region server web UI at :60030/rs-status
EXPOSE 60030

# HADOOP PORTS
EXPOSE 50020 50090 50070 50010 50075 8031 8032 8033 8040 8042 49707 22 8088 8030

# start hadoop & hbase
RUN ["/etc/bootstrap.sh", "-bash"]

