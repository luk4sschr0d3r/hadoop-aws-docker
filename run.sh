#!/bin/bash
#
# Script to start docker and update the /etc/hosts file to point to
# the hadoop-aws-docker container
#

test "$(whoami)" = "root" || { echo -e "Please execute using sudo:\n$ sudo $0"; exit 1; }

container=aptlygmbh/hadoop-aws-docker:hadoop240-hbase0.94.26

echo "Starting container"
rm -rf logs
mkdir -p logs


PUBLISH_PORTS="-p 2181:2181 -p 22:22222 -p 49707:49707 -p 50010:50010 -p 50020:50020 -p 50070:50070 -p 50075:50075 -p 50090:50090 -p 60000:60000 -p 60010:60010 -p 60020:60020 -p 60030:60030 -p 8030:8030 -p 8031:8031 -p 8032:8032 -p 8033:8033 -p 8040:8040 -p 8042:8042 -p 8088:8088 -p 9090:9090 -p 9095:9095 -p 9000:9000 "

id=$(docker run -d -v $PWD/logs:/opt/hbase/logs ${PUBLISH_PORTS} aptlygmbh/hadoop-aws-docker:hadoop240-hbase0.94.26 /etc/bootstrap.sh -d)

echo "Container has ID $id"

# Get the hostname and IP inside the container
docker inspect $id > config.json
docker_hostname=`python -c 'import json; c=json.load(open("config.json")); print c[0]["Config"]["Hostname"]'`
docker_ip=`python -c 'import json; c=json.load(open("config.json")); print c[0]["NetworkSettings"]["IPAddress"]'`
rm -f config.json

echo "Updating /etc/hosts to make hadoop-aws-docker point to $docker_ip ($docker_hostname)"
if grep 'hadoop-aws-docker' /etc/hosts >/dev/null; then
  sudo sed -i "s/^.*hadoop-aws-docker.*\$/$docker_ip hadoop-aws-docker $docker_hostname/" /etc/hosts
else
  sudo sh -c "echo '$docker_ip hadoop-aws-docker $docker_hostname' >> /etc/hosts"
fi

echo "Now connect to hadoop at localhost on the standard ports"
echo ""
echo "For docker status:"
echo "$ id=$id"
echo "$ docker ps $$id"
echo

echo "# To start a bash in this container, run:"
echo sudo docker exec -it $docker_hostname /bin/bash

echo "# Startup will take circa. 30 seconds. To check if ZooKeeper is running, execute this and expect 'imok'"
echo echo ruok \| nc $docker_hostname 2181

echo ".... Waiting 30 seconds now :)"
sleep 30

echo ruok | nc $docker_hostname 2181
echo

